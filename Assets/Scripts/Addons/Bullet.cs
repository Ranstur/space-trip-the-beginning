﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public int damage;
    [Space]
    [SerializeField] private float travelSpeed;
    [SerializeField] private float decayTimer;
    [Header("Sounds")]
    [SerializeField] private GameObject playerHitSound;
    [SerializeField] private GameObject enemyHitSound;

    private void Update()
    {
        transform.Translate(0, -Time.deltaTime * travelSpeed, 0);

        Destroy(gameObject, decayTimer);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Enemy enemy = collision.GetComponent<Enemy>();
        ShipController sc = collision.GetComponent<ShipController>();
        if (enemy != null)
        {
            enemy.TakeDamage(damage);
            GameObject hit = Instantiate(enemyHitSound);
            hit.transform.position = transform.position;
        }
        else if(sc != null)
        {
           sc.TakeDamage(damage);

            GameObject hit = Instantiate(playerHitSound);
            hit.transform.position = transform.position;
        }
        Destroy(gameObject);
    }
}

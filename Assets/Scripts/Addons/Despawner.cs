using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Despawner : MonoBehaviour
{
    [SerializeField] private float timer = 10;
    void Start()
    {
        Destroy(gameObject, timer);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarSpawner : MonoBehaviour
{
    [SerializeField] private float spawnRate;
    [Space]
    [SerializeField] private GameObject starPrefab;
    [Header("platform movement")]
    public float moveSpeed = 1;
    [SerializeField] private Transform start;
    [SerializeField] private Transform end;

    private Transform target;

    //we want to begin by targeting the next transform in the list, which is 1
    private bool toEnd;

    private void Start()
    {
        StartCoroutine(StartSpawning());
        //set the position at the very first node
        transform.position = start.position;
        target = end;
    } 

    IEnumerator StartSpawning()
    {
        GameObject star = Instantiate(starPrefab);
        star.transform.position = transform.position;
        star.transform.rotation = transform.rotation;

        yield return new WaitForSeconds(spawnRate);

        StartCoroutine(StartSpawning());
    }


    private void FixedUpdate()
    {
        if (!start || !end)
        {
            Debug.LogError("Hey randy, all the platforms aren't assigned to the list lol");
            return;
        }

        MoveToNextPlatform();
    }

    void MoveToNextPlatform()
    {
        //figure out what transform we are targetting
        //Transform targetTransform = end;

        //get the distance to the next platform
        float distance = Vector2.Distance(transform.position, target.position);

        //if the distance is close enough, then we can target the next platform.
        if (distance <= 0.1f)
        {
            changer(toEnd);
        }
        else //else we are not close enough, so move towards the current target platform
        {
            //move to target position
            transform.position = Vector2.MoveTowards(transform.position, target.position, moveSpeed * Time.deltaTime);
        }
    }
    private void changer(bool ended)
    {
        if(ended)
        {
            target = end;

            toEnd = false;
        }
        else
        {
            target = start;

            toEnd = true;
        }    
    }
}

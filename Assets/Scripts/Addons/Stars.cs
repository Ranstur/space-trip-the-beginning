﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stars : MonoBehaviour
{
    [SerializeField] private float speed;
    [Space]
    [SerializeField] private float despawnTime = 11;
    void Update()
    {
        transform.Translate(0, -Time.deltaTime * speed, 0);

        Destroy(gameObject, despawnTime);
    }
}

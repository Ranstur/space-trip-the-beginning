using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CompanyUI : MonoBehaviour
{
    [SerializeField] private Image nameCover;
    [Space]
    [SerializeField] private float coverTimer;

    private float buffer = 2.5f;
    private static bool played = false;
    private void Update()
    {
        nameCover.fillAmount -= Time.deltaTime * coverTimer / 100;

        if (nameCover.fillAmount == 0 || played)
        {
            played = true;
            Destroy(gameObject, buffer);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Computer : MonoBehaviour
{
    public bool mainPower;
    public bool secondaryPower;
    [Header(" how to power computer")]
    [SerializeField] private bool onlyMain;
    [SerializeField] private bool onlySecondary;
    [SerializeField] private bool useBoth;
    [Space]
    [SerializeField] private bool otherPower;
    [Space]
    [SerializeField] private GameObject computerSystemUI;
    [SerializeField] private BoxCollider2D boxC;
    [Header("sprites")]
    [SerializeField] private Sprite poweredComputer;
    [SerializeField] private Sprite unpoweredComputer;
    [Space]
    [SerializeField] private Navigation nav;
    [SerializeField] private bool restart;

    public bool isIn;
    private bool isUsing = false;
    private bool powered = true;

    private void Start()
    {
        if (mainPower)
        {
            SpriteRenderer sr = GetComponent<SpriteRenderer>();
            sr.sprite = poweredComputer;

            boxC.enabled = true;
        }
        if (!mainPower)
        {
            SpriteRenderer sr = GetComponent<SpriteRenderer>();
            sr.sprite = unpoweredComputer;

            boxC.enabled = false;
        }
        if (computerSystemUI != null)
        {
            computerSystemUI.SetActive(false);
        }

        if(otherPower)
        {
            powered = true;
        }
    }
    private void Update()
    {
        if (!otherPower)
        {
            if (onlyMain)
            {
                if (mainPower)
                {
                    powered = true;
                }
                if (!mainPower)
                {
                    powered = false;
                }
            }
            if (onlySecondary)
            {
                if (secondaryPower)
                {
                    powered = true;
                }
                if (!secondaryPower)
                {
                    powered = false;
                }
            }
            if (useBoth)
            {
                if (mainPower || secondaryPower)
                {
                    powered = true;
                }
                if (!mainPower && !secondaryPower)
                {
                    powered = false;
                }
            }
        }
        if(onlyMain && onlySecondary || onlyMain && useBoth || onlySecondary && useBoth)
        {
            Debug.LogError($"{gameObject.name} have more the 1 power is true.");
            return;
        }
        if(!onlyMain && !onlySecondary && !useBoth)
        {
            Debug.LogError($"{gameObject.name} have no way that it can get power!");
            return;
        }

        if (powered)
        {
            SpriteRenderer sr = GetComponent<SpriteRenderer>();
            sr.sprite = poweredComputer;

            boxC.enabled = true;
        }
        if (!powered)
        {
            SpriteRenderer sr = GetComponent<SpriteRenderer>();
            sr.sprite = unpoweredComputer;

            boxC.enabled = false;

            if (!restart)
            {
                //computerSystemUI.SetActive(false);
            }

            if (nav != null)
            {
                PlayerControler pc = FindObjectOfType<PlayerControler>();
                pc.canMove = true;
            }
        }

        if (Input.GetKeyDown(KeyCode.E) && isIn && !isUsing)
        {
            isUsing = true;

            PlayerControler pc = FindObjectOfType<PlayerControler>();
            pc.canMove = false;

            if (nav == null)
            {
                computerSystemUI.SetActive(true);
            }
        }
        else if(Input.GetKeyDown(KeyCode.E) && isUsing)
        {
            isUsing = false;

            PlayerControler pc = FindObjectOfType<PlayerControler>();
            pc.canMove = true;

            if (nav == null)
            {
                computerSystemUI.SetActive(false);
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        PlayerControler pc = collision.GetComponent<PlayerControler>();
        if (pc != null)
        {
            isIn = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        PlayerControler pc = collision.GetComponent<PlayerControler>();
        if(pc != null)
        {
            isIn = false;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyComputer : MonoBehaviour
{
    [SerializeField] private float hackingTime;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Enemy enemy = collision.GetComponent<Enemy>();
        if(enemy != null)
        {
            StartCoroutine(Hacking());
        }
    }
    IEnumerator Hacking()
    {
        CargoAirLockController calc = GetComponent<CargoAirLockController>();
        AlarmManager am = FindObjectOfType<AlarmManager>();

        yield return new WaitForSeconds(hackingTime);

        am.alarmCounter += 1;
        calc.running = true;
    }
}

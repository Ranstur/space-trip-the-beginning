﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ComputerUI : MonoBehaviour
{
    public bool isOn;
    [Space]
    [SerializeField] private string name;
    [SerializeField] private TextMeshProUGUI nameText;
    [Space]
    [SerializeField] private Button onButton;
    [SerializeField] private Button offButton;
    [Space]
    [SerializeField] private Sprite onColor;
    [SerializeField] private Sprite offColor;
    [Header("bools for what it does")]
    [SerializeField] private bool isAlarms;
    [SerializeField] private bool isTurrets;
    [SerializeField] private bool isMainPowerGrid;
    [SerializeField] private bool isUningPowerAirLock;
    [SerializeField] private bool isMasterAirLock;
    [SerializeField] private bool isFireGrid;
    [SerializeField] private bool isMainEnginePower;
    [SerializeField] private bool isSecondaryEnginePower;
    [SerializeField] private bool isManeuveringThrusterPower;
    [Space]
    [SerializeField] private bool triggerButton;
    [Header("Scripts")]
    [SerializeField] private List<ComputerUI> comUI;
    [SerializeField] private AirLockControls alc;
    [SerializeField] private Generator gen;
    [SerializeField] private MainPowerGrid mpg;
    [SerializeField] private LockDown ld;
    [SerializeField] private OxygenManager om;
    [SerializeField] private GravtiySystems gs;
    [SerializeField] private Shield shield;
    [SerializeField] private MainEngine me;
    [SerializeField] private SecondaryEngine se;
    [SerializeField] private ManeuveringThruster mt;
    [SerializeField] private Weapon weapon;
    [SerializeField] private AmmoFactory af;
    [SerializeField] private SecondaryPowerGrid spg;

    private bool begin = false;

    private void Start()
    {
        nameText.text = name;

        if(isOn)
        {
            OnOn();
        }
        
        if (!isOn)
        {
            OnOff();
        }

        onButton.onClick.AddListener(OnOn);
        offButton.onClick.AddListener(OnOff);
    }
    public void Update()
    {
        if(isOn)
        {
            Image image = onButton.GetComponent<Image>();
            image.sprite = onColor;

            Image image2 = offButton.GetComponent<Image>();
            image2.sprite = offColor;
        }
        if(!isOn)
        {
            Image image1 = offButton.GetComponent<Image>();
            image1.sprite = onColor;

            Image image = onButton.GetComponent<Image>();
            image.sprite = offColor;
        }
    }
    private void OnOn()
    {
        isOn = true;

        if(isAlarms)
        {
            AlarmManager am = FindObjectOfType<AlarmManager>();
            am.Activated();
        }
        if(isTurrets)
        {
            TurretManager tm = FindObjectOfType<TurretManager>();
            tm.Activated();
        }
        if(alc != null)
        {
            if (isUningPowerAirLock)
            {
                alc.PoweredUp();
            }
            else
            {
                alc.Opening();

                if (triggerButton &&!begin)
                {
                    begin = true;

                    NatralProblems natralProblems = FindObjectOfType<NatralProblems>();
                    natralProblems.begin = true;
                }
            }

            if(isMasterAirLock)
            {
                foreach(ComputerUI allSystems in comUI)
                {
                    allSystems.isOn = true;
                }
            }
        }
        if(isMainPowerGrid)
        {
            TurretManager tm = FindObjectOfType<TurretManager>();
            AlarmManager am = FindObjectOfType<AlarmManager>();

            tm.PoweredUp();
            am.PoweredUp();
            mpg.PoweredUp();
        }
        if(gen != null)
        {
            gen.isOn = true;
        }
        if(ld != null)
        {
            ld.StartLockDown();
        }
        if(om != null)
        {
            om.SetOverRide(false);
            om.PoweredUp();
        }
        if(isFireGrid)
        {
            FireGrid fg = FindObjectOfType<FireGrid>();
            fg.powered = true;
        }
        if(gs != null)
        {
            gs.running = true;
        }
        if (shield != null)
        {
            shield.powered = true;
        }
        if(me != null && isMainEnginePower)
        {
            me.powered = true;
        }
        if(me != null && !isMainEnginePower)
        {
            me.running = true;
        }
        if(se != null && isSecondaryEnginePower)
        {
            se.powered = true;
        }
        if(se != null && !isSecondaryEnginePower)
        {
            se.running = true;
        }
        if(mt != null && isManeuveringThrusterPower)
        {
            mt.powered = true;
        }
        if(mt != null && !isManeuveringThrusterPower)
        {
            mt.running = true;
        }
        if(weapon != null)
        {
            weapon.powered = true;
        }
        if(af != null)
        {
            af.powered = true;
        }
        if(spg != null)
        {
            spg.PoweredUp();
        }
    }
    private void OnOff()
    {
        isOn = false;

        if(isAlarms)
        {
            AlarmManager am = FindObjectOfType<AlarmManager>();
            am.Deactivated();
        }
        if(isTurrets)
        {
            TurretManager tm = FindObjectOfType<TurretManager>();
            tm.Deactivated();
        }
        if(alc != null)
        {
            if(isUningPowerAirLock)
            {
                alc.PoweredDown();
            }
            else  alc.Closing();

            if (isMasterAirLock)
            {
                foreach (ComputerUI allSystems in comUI)
                {
                    allSystems.isOn = false;
                }
            }
        }
        if(isMainPowerGrid)
        {
            TurretManager tm = FindObjectOfType<TurretManager>();
            AlarmManager am = FindObjectOfType<AlarmManager>();

            tm.PoweredDown();
            am.PoweredDown();
            mpg.PoweredDown();
        }
        if(gen != null)
        {
            gen.isOn = false;
        }
        if(ld != null)
        {
            ld.Unlock();
        }
        if(om != null)
        {
            om.SetOverRide(true);
            om.PoweredDown();
        }
        if(isFireGrid)
        {
            FireGrid fg = FindObjectOfType<FireGrid>();
            fg.powered = false;
        }
        if(gs != null)
        {
            gs.running = false;
        }
        if(shield != null)
        {
            shield.powered = false;
        }
        if(me != null && isMainEnginePower)
        {
            me.powered = false;
        }
        if(me != null && !isMainEnginePower)
        {
            me.running = false;
        }
        if(se != null && isSecondaryEnginePower)
        {
            se.powered = false;
        }
        if(se != null && !isSecondaryEnginePower)
        {
            se.running = false;
        }
        if(mt != null && isManeuveringThrusterPower)
        {
            mt.powered = false;
        }
        if(mt != null && !isManeuveringThrusterPower)
        {
            mt.running = false;
        }
        if(weapon != null)
        {
            weapon.powered = false;
        }
        if(af != null)
        {
            af.powered = false;
        }
        if (spg != null)
        {
            spg.PoweredDown();
        }
    }
}

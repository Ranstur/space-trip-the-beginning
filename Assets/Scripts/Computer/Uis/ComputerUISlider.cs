﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ComputerUISlider : MonoBehaviour
{
    [SerializeField] private Slider bar;
    [SerializeField] private TextMeshProUGUI barText;
    [Space]
    [SerializeField] private float minValue = 0;
    [SerializeField] private float maxValue = 1;
    [Space]
    [SerializeField] private string name;
    [SerializeField] private TextMeshProUGUI text;
    [Header("Scripts")]
    [SerializeField] private OxygenManager om;
    [SerializeField] private Battery mainBattery;
    [SerializeField] private Battery sceBattery;

    private void Start()
    {
        bar.minValue = minValue;
        bar.maxValue = maxValue;

        text.text = name;
    }
    private void Update()
    {
        if (om != null)
        {
            bar.value = om.oxygenCount / 100;
            barText.text = $"{om.oxygenCount}%";
        }
        if(mainBattery != null)
        {
            bar.value = mainBattery.chargeLeft / 100;
            barText.text = $"{mainBattery.chargeLeft}%";
        }
        if(sceBattery != null)
        {
            bar.value = sceBattery.chargeLeft / 100;
            barText.text = $"{sceBattery.chargeLeft}%";
        }
    }
}

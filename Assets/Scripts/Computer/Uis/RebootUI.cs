﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RebootUI : MonoBehaviour
{
    public bool isOn;
    [SerializeField] private bool useBool;
    [Space]
    [SerializeField] private string name;
    [SerializeField] private TextMeshProUGUI nameText;
    [Space]
    [SerializeField] private Button rebootButton;
    [Space]
    [SerializeField] private Sprite onColor;
    [SerializeField] private Sprite offColor;
    [Header("Scripts")]
    [SerializeField] private Generator gen;
    [SerializeField] private Computer backUpComputer;
    [SerializeField] private ComputerUI generatorButton;
    [SerializeField] private GravtiySystems gravtiySystem;
    [SerializeField] private Shield shield;
    [SerializeField] private AmmoFactory af;

    private void Start()
    {
        nameText.text = name;

        rebootButton.onClick.AddListener(OnReboot);

        Image image = rebootButton.GetComponent<Image>();
        image.sprite = offColor;
    }
    private void Update()
    {
        if(useBool)
        {
            if(isOn)
            {
                Image image = rebootButton.GetComponent<Image>();
                image.sprite = onColor;
            }
            if(!isOn)
            {
                Image image = rebootButton.GetComponent<Image>();
                image.sprite = offColor;
            }
        }
    }
    private void OnReboot()
    {
        Image image = rebootButton.GetComponent<Image>();
        image.sprite = onColor;

        if (gen != null)
        {
            gen.isOn = true;

            generatorButton.isOn = true;
        }
        if(gravtiySystem != null)
        {
            gravtiySystem.malfunctioning = false;
        }
        if(shield != null)
        {
            shield.running = true;
        }
        if(af != null)
        {
            af.powered = true;
        }
        StartCoroutine(Change());
    }
    IEnumerator Change()
    {
        yield return new WaitForFixedUpdate();

        if (!useBool)
        {
            Image image = rebootButton.GetComponent<Image>();
            image.sprite = offColor;
        }
    }
}

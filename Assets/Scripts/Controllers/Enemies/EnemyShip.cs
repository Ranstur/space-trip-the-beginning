using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShip : MonoBehaviour
{
    [SerializeField] private float speed;
    [Space]
    [SerializeField] private Transform start;
    [SerializeField] private Transform end;
    [Space]
    [SerializeField] private Transform leftShotSpot;
    [SerializeField] private Transform rightShotSpot;
    [Space]
    [SerializeField] private GameObject bulletPrefab;
    [SerializeField] private float shotDelay;
    [Header("sounds")]
    [SerializeField] private AudioSource shoot;

    private Transform target;
    private bool toEnd;

    void Start()
    {
        transform.position = start.position;
        target = end;

        StartCoroutine(Shooting(false));
    }
    IEnumerator Shooting(bool isLeft)
    {
        shoot.Play();

        if (!isLeft)
        {
            GameObject bullet = Instantiate(bulletPrefab);
            bullet.transform.position = rightShotSpot.transform.position;
            bullet.transform.rotation = rightShotSpot.transform.rotation;
        }
        else
        {
            GameObject bullet = Instantiate(bulletPrefab);
            bullet.transform.position = leftShotSpot.transform.position;
            bullet.transform.rotation = leftShotSpot.transform.rotation;
        }
        yield return new WaitForSeconds(shotDelay);

        StartCoroutine(Shooting(!isLeft));
    }

    #region Movement
    private void FixedUpdate()
    {
        if (!start || !end)
        {
            Debug.LogError("Hey randy, all the platforms aren't assigned to the list lol");
            return;
        }

        MoveToNextPlatform();
    }
    void MoveToNextPlatform()
    {
        //figure out what transform we are targetting
        //Transform targetTransform = end;

        //get the distance to the next platform
        float distance = Vector2.Distance(transform.position, target.position);

        //if the distance is close enough, then we can target the next platform.
        if (distance <= 0.1f)
        {
            changer(toEnd);
        }
        else //else we are not close enough, so move towards the current target platform
        {
            //move to target position
            transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        }
    }
    private void changer(bool ended)
    {
        if (ended)
        {
            target = end;

            toEnd = false;
        }
        else
        {
            target = start;

            toEnd = true;
        }
    }
    #endregion
}

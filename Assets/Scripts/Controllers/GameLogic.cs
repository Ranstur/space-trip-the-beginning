﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SubtitleFont
{
    public string fontName;
    [Space]
    public AudioSource sound;
}
public class GameLogic : MonoBehaviour
{
    public bool lockedOut = false;
    [SerializeField] private int min = 1;
    [SerializeField] private int max = 10;
    [Header("Scripts")]
    [SerializeField] private AlarmManager am;
    [SerializeField] private List<TurretManager> tm;
    [SerializeField] private List<AirLockControls> hallwayALC;
    [SerializeField] private List<AirLockControls> doorwayALC;
    [SerializeField] private Generator mainGenerator;
    [SerializeField] private Generator secGenerator;
    [SerializeField] private OxygenManager om;
    [SerializeField] private FireGrid fg;
    [SerializeField] private MainPowerGrid mpg;
    [SerializeField] private GravtiySystems gs;
    [SerializeField] private MainEngine me;
    [SerializeField] private SecondaryEngine se;
    [SerializeField] private ManeuveringThruster mt;
    [SerializeField] private Weapon weapon;
    [SerializeField] private AmmoFactory af;
    [SerializeField] private Shield shield;
    [SerializeField] private SecondaryPowerGrid spg;

    [SerializeField] private List<SubtitleFont>audio;

    private int lastNumber;

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.L))
        {
            RandomGenerator();
        }
        if(Input.GetKeyDown(KeyCode.H))
        {
            Debug.Log("Audio List Goes!" +
                "  0: Alarms" +
                "  1: Turrets" +
                "  2: Hallway Air Locks" +
                "  3: Doorway Air Locks" +
                "  4: Main Generator" +
                "  5: Sec Generator" +
                "  6: Oxygen" +
                "  7: Fire Grid" +
                "  8: Main Power Grid" +
                "  9: Gravity Not running" +
                " 10: Gravtiy Malfunctioning" +
                " 11: Main Engine" +
                " 12: Secondary Engine" +
                " 13: Maneuvering Thruster" +
                " 14: Weapon" +
                " 15: Ammo Factory" +
                " 16: Shield" +
                " 17: Secondary Power Grid");
        }
    }
    public void RandomGenerator()
    {
        if (!lockedOut)
        {
            int number = Random.Range(min, max);

            if (number == lastNumber)
            {
                RandomGenerator();
                return;
            }
            else
            {
                lastNumber = number;
            }

            if (number == 0)
            {
                Debug.LogError("you can't use ZERO");
                min = 1;
            }
            if (number == 1)
            {
                am.Deactivated();
                audio[0].sound.Play();
            }
            if (number == 2)
            {
                foreach (TurretManager allTurrets in tm)
                {
                    allTurrets.Deactivated();
                }
                audio[1].sound.Play();
            }
            if (number == 3)
            {
                foreach (AirLockControls allAirLocks in hallwayALC)
                {
                    allAirLocks.PoweredDown();
                }
                audio[2].sound.Play();
            }
            if (number == 4)
            {
                mainGenerator.isOn = false;
                audio[4].sound.Play();
            }
            if (number == 5)
            {
                secGenerator.isOn = false;
                audio[5].sound.Play();
            }
            if (number == 6)
            {
                foreach (AirLockControls allAirlocks in doorwayALC)
                {
                    allAirlocks.PoweredDown();
                }
                audio[3].sound.Play();
            }
            if (number == 7)
            {
                om.SetOverRide(true);
                om.PoweredDown();
                audio[6].sound.Play();
            }
            if (number == 8)
            {
                fg.powered = false;
                audio[7].sound.Play();
            }
            if (number == 9)
            {
                mpg.PoweredDown();
                audio[8].sound.Play();
            }
            if (number == 10)
            {
                gs.running = false;
                audio[9].sound.Play();
            }
            if (number == 11)
            {
                gs.malfunctioning = true;
                audio[10].sound.Play();
            }
            if (number == 12)
            {
                me.powered = false;
                me.running = false;
                audio[11].sound.Play();
            }
            if (number == 13)
            {
                se.powered = false;
                se.running = false;
                audio[12].sound.Play();
            }
            if (number == 14)
            {
                mt.powered = false;
                mt.running = false;
                audio[13].sound.Play();
            }
            if (number == 15)
            {
                weapon.powered = false;
                audio[14].sound.Play();
            }
            if (number == 16)
            {
                af.powered = false;
                audio[15].sound.Play();
            }
            if (number == 17)
            {
                shield.powered = false;
                //audio[16].sound.Play();
            }
            if (number == 18)
            {
                spg.PoweredDown();
                //audio[17].sound.Play();
            }
            if (number >= 19)
            {
                Debug.LogError("the max is too high or you need to add more code.");
            }
            Debug.Log($"{number}");
            //Note: you must take 1 away from max for the code to work 
            //Ex: 9 number = 10 max.
        }
    }
}

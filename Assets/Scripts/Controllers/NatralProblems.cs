﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class rockSpawner
{
    public string fontName;
    [Space]
    public float drainTime;
}

public class NatralProblems : MonoBehaviour
{
    public bool lockedOut = false;
    public bool begin;
    [SerializeField] private float problemTimer;
    [Header("ship problem")]
    [SerializeField] private float startTimer;
    [SerializeField] private float timer;
    [SerializeField] private float audioTime;
    [Space]
    [SerializeField] private List<rockSpawner> waves;
    [Header("Whole Game Settings")]
    [SerializeField] private float gameTimer = 120;
    [Header("Sounds")]
    [SerializeField] private AudioSource rocksIncoming;

    private AsteroidManager am;
    private GameLogic gameLogic;
    private int target;
    private bool locked;
    private bool first;

    private void Start()
    {
        gameLogic = FindObjectOfType<GameLogic>();
        am = FindObjectOfType<AsteroidManager>();

        if (startTimer - audioTime <= 0)
        {
            Debug.LogError("The Start Timer is too low or the audio Timer is too high.");

            return;
        }
    }
    public void Update()
    {
        if (begin && !locked)
        {
            locked = true;

            StartCoroutine(Problem());
            StartCoroutine(ShipProblem());
            StartCoroutine(WholeGameTiming());
            StartCoroutine(Audio());
        }

        if(lockedOut)
        {
            StopAllCoroutines();
        }
    }
    IEnumerator Problem()
    {
        yield return new WaitForSeconds(problemTimer);

        gameLogic.RandomGenerator();

        StartCoroutine(Problem());
    }
    IEnumerator ShipProblem()
    { 
        yield return new WaitForSeconds(startTimer);

        Spawning();
    }
    void Spawning()
    {
        float change;

        change = waves[target].drainTime;

        StartCoroutine(Drain(change));
        StartCoroutine(Audio());

        target++;
    }
    IEnumerator Drain(float drain)
    {
        am.TurnOn();

        yield return new WaitForSeconds(drain);

        am.TurnOff();

        StartCoroutine(breaker());
    }
    IEnumerator breaker()
    {
        yield return new WaitForSeconds(timer);

        Spawning();
    }
    IEnumerator Audio()
    {
        float trigger;

        if (!first)
        {
            first = true;

            trigger = startTimer - audioTime;
        }
        else
        {
            trigger = timer - audioTime;

            yield return new WaitForSeconds(trigger);
        }

        rocksIncoming.Play();
    }
    IEnumerator WholeGameTiming()
    {
        yield return new WaitForSeconds(gameTimer);

        AttackStory attackStory = FindObjectOfType<AttackStory>();
        attackStory.starting = true;
    }
}

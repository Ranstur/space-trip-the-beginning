﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipController : MonoBehaviour
{
    public bool canMove = false;
    public bool lockedMove = false;
    [Space]
    [SerializeField] private float health = 100;
    [SerializeField] private float selfHealingTimer;
    [SerializeField] private float speed;
    [Space]
    [SerializeField] private Shield shield;
    [SerializeField] private MainEngine me;
    [SerializeField] private SecondaryEngine se;
    [SerializeField] private ManeuveringThruster mt;
    [SerializeField] private Weapon weapon;
    [Header("Sprites")]
    [SerializeField] private Sprite allWorking;
    [SerializeField] private Sprite nothingWorking;
    [Header("Main Engein and other not working")]
    [SerializeField] private Sprite justMainEngine;
    [SerializeField] private Sprite mManeuveringThrusters;
    [Header("Secondary Engine and other not working")]
    [SerializeField] private Sprite justSecEngine;
    [SerializeField] private Sprite sManeuveringThrusters;
    [Header("maneuvering Thruster and other not working")]
    [SerializeField] private Sprite justManeuveringThruster;
    [SerializeField] private Sprite mMainEngine;
    [SerializeField] private Sprite mSecEngine;

    private SpriteRenderer sr;
    private void Start()
    {
        sr = GetComponent<SpriteRenderer>();

        StartCoroutine(Healing());
        speed = mt.movementSpeed;
    }
    private void Update()
    {
        if(canMove && lockedMove)
        {
            if(Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
            {
                transform.Translate(-Time.deltaTime * speed, 0, 0);
            }
            if(Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
            {
                transform.Translate(Time.deltaTime * speed, 0, 0);
            }
        }
        if(mt.activated)
        {
            canMove = true;
        }
        if(!mt.activated)
        {
            canMove = false;
        }

        if(mt.activated && me.activated && se.activated)
        {
            sr.sprite = allWorking;
        }
        else if(mt.activated && me.activated && !se.activated)
        {
            sr.sprite = mMainEngine;
        }
        else if(mt.activated && !me.activated && !se.activated)
        {
            sr.sprite = justManeuveringThruster;
        }
        else if(!mt.activated && me.activated && se.activated)
        {
            sr.sprite = mManeuveringThrusters;
        }
        else if(!mt.activated && !me.activated && se.activated)
        {
            sr.sprite = justSecEngine;
        }
        else if(!mt.activated && me.activated && !se.activated)
        {
            sr.sprite = justMainEngine;
        }
        else if(mt.activated && !me.activated && se.activated)
        {
            sr.sprite = mSecEngine;
        }
        else if(!mt.activated && !me.activated && !se.activated)
        {
            sr.sprite = nothingWorking;
        }
    }
    public void TakeDamage(int amount)
    {
        if (!shield.running)
        {
            health -= amount;

            if (health <= 0)
            {
                GameLogic gameLogic = FindObjectOfType<GameLogic>();
                gameLogic.RandomGenerator();

                health = 100;
            }
        }
    }
    IEnumerator Healing()
    {
        yield return new WaitForSeconds(selfHealingTimer);

        if (health >= 100)
        {
            health = 100;
        }
        else
        {
            health++;
        }

        StartCoroutine(Healing());
    }
}

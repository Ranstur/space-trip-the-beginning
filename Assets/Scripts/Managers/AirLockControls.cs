﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirLockControls : MonoBehaviour
{
    [SerializeField] private List<InnerAirLockDoor> airLocks;
    [SerializeField] private ComputerUI comUIPower;

    public void Opening()
    {
        foreach(InnerAirLockDoor allDoors in airLocks)
        {
            allDoors.isclosed = false;
        }
    }
    public void Closing()
    {
        foreach (InnerAirLockDoor allDoors in airLocks)
        {
            allDoors.isclosed = true;
        }
    }
    public void PoweredUp()
    {
        foreach (InnerAirLockDoor allDoors in airLocks)
        {
            allDoors.powered = true;
        }
        if (comUIPower != null)
        {
            comUIPower.isOn = true;
        }
    }
    public void PoweredDown()
    {
        foreach (InnerAirLockDoor allDoors in airLocks)
        {
            allDoors.powered = false;
        }
        if (comUIPower != null)
        {
            comUIPower.isOn = false;
        }
    }
}

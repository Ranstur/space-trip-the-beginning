﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlarmManager : MonoBehaviour
{
    [SerializeField] private List<AlarmLights> alarms;
    [SerializeField] private AlarmUI alarmUI;
    [SerializeField] private ComputerUI comUI;
    [Space]
    public int alarmCounter;
    [Space]
    public bool mainPower;
    public bool secPower;

    private void Update()
    {
        if(alarmCounter >= 1)
        {
            TurnOn();
        }
        else
        {
            TurnOff();
        }
        if(!mainPower && !secPower)
        {
            PoweredDown();
        }
        if(mainPower || secPower)
        {
            PoweredUp();
        }
    }
    public void Activated()
    {
        foreach(AlarmLights allAlarms in alarms)
        {
            allAlarms.isActivated = true;
        }
        comUI.isOn = true;
    }
    public void Deactivated()
    {
        foreach(AlarmLights allAlarms in alarms)
        {
            allAlarms.isActivated = false;
        }
        comUI.isOn = false;
    }
    public void TurnOn()
    {
        foreach(AlarmLights allAlarms in alarms)
        {
            allAlarms.running = true;
        }
        alarmUI.running = true;
    }
    public void TurnOff()
    {
        foreach(AlarmLights allAlarms in alarms)
        {
            allAlarms.running = false;
        }
        alarmUI.running = false;
    }
    public void PoweredDown()
    {
        foreach(AlarmLights allAlarms in alarms)
        {
            allAlarms.powered = false;
        }
    }
    public void PoweredUp()
    {
        foreach(AlarmLights allAlarms in alarms)
        {
            allAlarms.powered = true;
        }
    }
}

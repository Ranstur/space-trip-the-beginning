﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AmmoFactory : MonoBehaviour
{
    public bool powered;
    public int currentAmmo;
    [Space]
    [SerializeField] private float craftSpeed;
    [SerializeField] private int amountCrafted;
    [Space]
    [SerializeField] private int maxAmmo;
    [Header("UIs")]
    [SerializeField] private TextMeshProUGUI ammoText;
    [SerializeField] private ComputerUI comUI;

    private bool locked;

    private void Update()
    {
        ammoText.text = $"{currentAmmo} / {maxAmmo}";

        if (powered)
        {
            comUI.isOn = true;

            if (currentAmmo <= maxAmmo)
            {
                if (!locked)
                {
                    locked = true;

                    BootUp();
                }
            }
            if (currentAmmo >= maxAmmo)
            {
                currentAmmo = maxAmmo;

                StopAllCoroutines();
            }
        }
        if(!powered)
        {
            comUI.isOn = false;
        }
    }
    public void BootUp()
    {
        StartCoroutine(Crafting());
    }
    IEnumerator Crafting()
    {
        yield return new WaitForSeconds(craftSpeed);

        currentAmmo++;

        if (currentAmmo <= maxAmmo)
        {
            StopAllCoroutines();
            StartCoroutine(Crafting());
        }
        if (locked)
        {
            locked = false;
        }
    }
}

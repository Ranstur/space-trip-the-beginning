﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidManager : MonoBehaviour
{
    [SerializeField] private List<AsteriodSpawner> spawners;
    public void TurnOn()
    {
        foreach(AsteriodSpawner allspawners in spawners)
        {
            allspawners.startSpawning = true;
        }
    }
    public void TurnOff()
    {
        foreach(AsteriodSpawner allSpawners in spawners)
        {
            allSpawners.startSpawning = false;
        }
    }
}

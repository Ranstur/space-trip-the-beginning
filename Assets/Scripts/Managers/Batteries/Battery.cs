﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Battery : MonoBehaviour
{
    public float chargeLeft;
    [Space]
    [SerializeField] private float maxPower = 100;
    [Space]
    [SerializeField] private float chargeRate;
    [SerializeField] private float drainRate;
    [Space]
    [SerializeField] private bool mainPower;
    [SerializeField] private bool secPower;
    [Space]
    [SerializeField] private Computer mainComputer;
    [Space]
    [SerializeField] private Sprite deadBattery;
    [SerializeField] private Sprite lowBattery;
    [SerializeField] private Sprite middleBattery;
    [SerializeField] private Sprite fullBattery;

    private bool locked;
    private float lowlife;
    private float middleLife;
    private void Start()
    {
        chargeLeft = maxPower;
        lowlife = maxPower / 3;
        middleLife = lowlife * 2;
    }
    private void Update()
    {
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        if(chargeLeft <= 0)
        {
            mainComputer.mainPower = false;

            sr.sprite = deadBattery;

            chargeLeft = 0;

            if (mainPower)
            {
                MainPowerGrid mpg = FindObjectOfType<MainPowerGrid>();
                mpg.PoweredDown();
            }
            if(secPower)
            {

            }
        }
        else if(chargeLeft <= lowlife)
        {
            sr.sprite = lowBattery;
        }
        else if(chargeLeft <= middleLife)
        {
            sr.sprite = middleBattery;
        }
        else if(chargeLeft >= maxPower)
        {
            sr.sprite = fullBattery;

            chargeLeft = maxPower;
        }
        if(chargeLeft >= 1)
        {
            mainComputer.mainPower = true;
        }
    }
    public void PoweringUp()
    {
        StopAllCoroutines();
        StartCoroutine(Charging());
    }
    public void PoweringDown()
    {
        StopAllCoroutines();
        StartCoroutine(Decharging());
    }
    IEnumerator Charging()
    {
        yield return new WaitForSeconds(chargeRate);

        chargeLeft += 1;

        if (chargeLeft <= maxPower)
        {
            StartCoroutine(Charging());
        }
    }
    IEnumerator Decharging()
    {
        yield return new WaitForSeconds(drainRate);

        chargeLeft -= 1;

        if(chargeLeft >= 0)
        {
            StartCoroutine(Decharging());
        }
    }
}

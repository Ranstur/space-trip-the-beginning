﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShieldBattery : MonoBehaviour
{
    public bool powered;
    [Space]
    [SerializeField] private float maxCharge;
    [SerializeField] private float chargeRate;
    [SerializeField] private float drainRate;
    [Space]
    [SerializeField] private Sprite deadBattery;
    [SerializeField] private Sprite lowBattery;
    [SerializeField] private Sprite middleBattery;
    [SerializeField] private Sprite fullBattery;
    [Space]
    [SerializeField] private Shield shield;
    [Header("Computer UIs")]
    [SerializeField] private Image image1;
    [SerializeField] private Image image2;

    private float currentCharge;

    private bool locked = false;

    private bool charge;
    private bool drain;
    private float lowLife;
    private float middleLife;
    private SpriteRenderer sr;


    private void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        lowLife = maxCharge / 3;
        middleLife = lowLife * 2;
    }
    private void Update()
    {
        UIDesplay(currentCharge);

        if (currentCharge <= 0 && !locked)
        {
            locked = true;

            currentCharge = 0;

            sr.sprite = deadBattery;

            shield.running = false;

            shield.shield.SetActive(false);

            ChargeUp();
        }
        else if (currentCharge <= lowLife)
        {
            sr.sprite = lowBattery;
        }
        else if (currentCharge <= middleLife)
        {
            sr.sprite = middleBattery;
        }
        else if(currentCharge >= maxCharge)
        {
            currentCharge = maxCharge;

            sr.sprite = fullBattery;

            shield.charged = true;
        }

        if(!powered)
        {
            StopAllCoroutines();
        }
        else if(powered)
        {
            if(charge)
            {
                StartCoroutine(Charging());
            }
            if(drain)
            {
                StartCoroutine(Draining());
            }
        }
    }
    private void UIDesplay(float amount)
    {
        float newAmount = amount / 100;

        image1.fillAmount = newAmount;
        image2.fillAmount = newAmount;
    }
    public void ChargeUp()
    {
            StartCoroutine(Charging());
    }
    public void DrainDown()
    {
        StartCoroutine(Draining());
    }
    IEnumerator Charging()
    {
        charge = true;
        drain = false;

        yield return new WaitForSeconds(chargeRate);

        currentCharge++;

        locked = false;

        if(currentCharge <= maxCharge)
        {
            StopAllCoroutines();
            StartCoroutine(Charging());
        }
    }
    IEnumerator Draining()
    {
        drain = true;
        charge = false;

        yield return new WaitForSeconds(drainRate);

        currentCharge--;

        if(currentCharge >= 0)
        {
            StartCoroutine(Draining());
        }
    }
}

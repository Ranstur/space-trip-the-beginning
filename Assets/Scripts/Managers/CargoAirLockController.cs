﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CargoAirLockController : MonoBehaviour
{
    public bool running;
    [SerializeField] private OuterAirlockDoors outterDoor;
    [SerializeField] private OuterAirlockDoors innerDoor;
    [Space]
    [SerializeField] private float doorTimer;
    [SerializeField] private float delayTimer;

    private void Update()
    {
        if(running)
        {
            running = false;
            StartCoroutine(DoorTiming());
        }
    }
    IEnumerator DoorTiming()
    {
        outterDoor.isOpen = true;

        yield return new WaitForSeconds(doorTimer);

        outterDoor.isOpen = false;

        yield return new WaitForSeconds(delayTimer);

        innerDoor.isOpen = true;

        yield return new WaitForSeconds(doorTimer);

        innerDoor.isOpen = false;
    }
}

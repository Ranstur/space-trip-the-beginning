﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireGrid : MonoBehaviour
{
    public bool powered;
    [Space]
    [SerializeField] private ComputerUI comUI;

    private bool locker;
    private void Update()
    {
        if (!powered && !locker)
        {
            locker = true;

            comUI.isOn = false;
        }
        if (powered && locker)
        {
            locker = false;

            comUI.isOn = true;
        }
    }
}

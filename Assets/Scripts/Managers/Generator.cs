﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generator : MonoBehaviour
{
    public bool makingPowered;
    public bool isOn;
    [Space]
    [SerializeField] private float startUpTime;
    [SerializeField] private float ShutDownTime;
    [Space]
    [SerializeField] private Computer computer;
    [SerializeField] private Computer backUpComputer;
    [Space]
    [SerializeField] private bool mainPower;
    [SerializeField] private bool secPower;
    [Space]
    [SerializeField] private ComputerUI system;
    [SerializeField] private Battery battery;

    private bool controlLock;
    private bool poweredDown;
    private AlarmManager am;

    public void Update()
    {
        am = FindObjectOfType<AlarmManager>();

        if(isOn && !makingPowered && !controlLock)
        {
            StartCoroutine(StartUp());
        }
        else if(!isOn && makingPowered && !controlLock)
        {
            StartCoroutine(ShutDown());
        }
        if(poweredDown)
        {
            backUpComputer.mainPower = true;
        }
        if(!poweredDown)
        {
            backUpComputer.mainPower = false;
        }
    }
    public void PoweredUp()
    {
        computer.mainPower = true;

        if (mainPower)
        {
            MainPowerGrid mpg = FindObjectOfType<MainPowerGrid>();
            mpg.PoweredUp();
        }
        if (mainPower)
        {
            am.alarmCounter -= 1;
        }

        if (secPower)
        {
            SecondaryPowerGrid spg = FindObjectOfType<SecondaryPowerGrid>();
            spg.PoweredUp();
        }
    }
    public void PoweredDown()
    {
        computer.mainPower = false;

        if (mainPower)
        {
            am.alarmCounter += 1;
        }
    }
    IEnumerator StartUp()
    {
        controlLock = true;
        poweredDown = false;

        yield return new WaitForSeconds(startUpTime);

        makingPowered = true;
        
        PoweredUp();
        battery.PoweringUp();

        controlLock = false;
    }
    IEnumerator ShutDown()
    {
        controlLock = true;

        poweredDown = true;
        makingPowered = false;

        PoweredDown();
        battery.PoweringDown();

        yield return new WaitForSeconds(ShutDownTime);

        controlLock = false;
    }
}

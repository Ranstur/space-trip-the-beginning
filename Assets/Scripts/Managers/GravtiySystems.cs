﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravtiySystems : MonoBehaviour
{
    public bool powered;
    public bool running;
    public bool malfunctioning;
    [Space]
    [SerializeField] private float timing;
    [SerializeField] private float coolDownTimer = 5;
    [Header("ComputerUI")]
    [SerializeField] private ComputerUI comUI;
    [SerializeField] private RebootUI bootUI;

    private bool locked;
    private bool locked1;
    private float savedMovementSpeed;

    private bool slows;

    private PlayerControler pc;
    private void Start()
    {
        pc = FindObjectOfType<PlayerControler>();
        savedMovementSpeed = pc.movementSpeed;
    }
    private void Update()
    {
        if(running)
        {
            comUI.isOn = true;
        }
        if(!running)
        {
            comUI.isOn = false;
        }
        if(malfunctioning)
        {
            bootUI.isOn = false;
        }
        if(!malfunctioning)
        {
            bootUI.isOn = true;
        }

        if (!powered && !locked1 || !running && !locked1)
        {
            pc.movementSpeed = pc.movementSpeed / 2;
            locked1 = true;
        }
        if (powered && running && locked1)
        {
            pc.movementSpeed = savedMovementSpeed;
            locked1 = false;
        }
        if(malfunctioning && !locked)
        {
            locked = true;
            StartCoroutine(Timer(true));
        }
        if(!malfunctioning && locked)
        {
            locked = true;
            StopAllCoroutines();
            pc.movementSpeed = savedMovementSpeed;
        }
    }
    IEnumerator Timer(bool slow)
    { 
        slow = slows;

        yield return new WaitForSeconds(timing);

        if(slow)
        {
            pc.movementSpeed = pc.movementSpeed / 2;
            slow = false;
        }
        if(!slow)
        {
            pc.movementSpeed = pc.movementSpeed / 4;
            slow = true;
        }

        StartCoroutine(CoolDown());
    }
    IEnumerator CoolDown()
    {
        yield return new WaitForSeconds(coolDownTimer);

        pc.movementSpeed = savedMovementSpeed;

        StartCoroutine(Timer(!slows));
    }

}

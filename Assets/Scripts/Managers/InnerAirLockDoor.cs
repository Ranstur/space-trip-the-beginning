﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InnerAirLockDoor : MonoBehaviour
{
    public bool powered;
    public bool isclosed;
    [Space]
    [SerializeField] private Sprite open;
    [SerializeField] private Sprite closed;
    [Space]
    [SerializeField] private BoxCollider2D boxC;

    private void Start()
    {
        boxC = GetComponent<BoxCollider2D>();

        if (powered)
        {
            if (isclosed)
            {
                DoorClosed();
            }
            if (!isclosed)
            {
                DoorOpen();
            }
        }
    }
    private void Update()
    {
        if (powered)
        {
            if (isclosed)
            {
                DoorClosed();
            }
            if (!isclosed)
            {
                DoorOpen();
            }
        }
    }
    private void DoorClosed()
    {
        boxC.enabled = true;

        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        sr.sprite = closed;
    }
    private void DoorOpen()
    {
        boxC.enabled = false;

        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        sr.sprite = open;
    }
}

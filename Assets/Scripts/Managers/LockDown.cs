﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockDown : MonoBehaviour
{
    [SerializeField] private float delayToNextDoor;
    [Space]
    [SerializeField] private List<InnerAirLockDoor> innerDoors;
    [Space]
    [SerializeField] private List<InnerAirLockDoor> dontAdd;

    private int targetDoor = 0;

    private void Start()
    {
        InnerAirLockDoor[] alldoors = FindObjectsOfType<InnerAirLockDoor>();
        innerDoors.AddRange(alldoors);

        if(innerDoors.Exists(dontAdd.Contains))
        {
            innerDoors.RemoveAll(dontAdd.Contains);
        }
    }
    public void StartLockDown()
    {
        StartCoroutine(Lock());
    }
    private void ResetAll()
    {
        targetDoor = 0;
    }
    private IEnumerator Lock()
    {
        innerDoors[targetDoor].isclosed = true;

        yield return new WaitForSeconds(delayToNextDoor);

        if (targetDoor <= innerDoors.Count)
        {
            targetDoor++;
            StartCoroutine(Lock());
        }
        else
        {
            ResetAll();
        }
    }
    public void Unlock()
    {
        StopAllCoroutines();
        ResetAll();
        foreach(InnerAirLockDoor allAirlockDoors in innerDoors)
        {
            allAirlockDoors.isclosed = false;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainPowerGrid : MonoBehaviour
{
    public bool powered;
    public bool powerOff;
    [Space]
    [SerializeField] private AirLockControls alc;
    [Space]
    [SerializeField] private List<Computer> computers;
    [SerializeField] private ComputerUI comUI;

    private void Start()
    {
        PoweredUp();
    }
    public void PoweredUp()
    {
        powerOff = false;
        comUI.isOn = true;

        TurretManager tm = FindObjectOfType<TurretManager>();
        tm.mainPower = true;

        AlarmManager am = FindObjectOfType<AlarmManager>();
        am.mainPower = true;

        alc.PoweredUp();

        OxygenManager om = FindObjectOfType<OxygenManager>();
        om.mainPower = true;

        FireGrid fg = FindObjectOfType<FireGrid>();
        fg.powered = true;

        foreach(Computer allComputers in computers)
        {
            allComputers.mainPower = true;
        }
    }
    public void PoweredDown()
    {
        powerOff = true;
        comUI.isOn = false;

        TurretManager tm = FindObjectOfType<TurretManager>();
        tm.mainPower = false;

        AlarmManager am = FindObjectOfType<AlarmManager>();
        am.mainPower = false;

        alc.PoweredDown();

        OxygenManager om = FindObjectOfType<OxygenManager>();
        om.mainPower = false;

        FireGrid fg = FindObjectOfType<FireGrid>();
        fg.powered = false;

        foreach(Computer allComputers in computers)
        {
            allComputers.mainPower = false;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Navigation : MonoBehaviour
{
    [SerializeField] private GameObject shipCam;
    [SerializeField] private GameObject playerCam;
    [Header("Scripts")]
    [SerializeField] private ShipController sc;
    [SerializeField] private Weapon weapon;

    private bool isIn;
    private bool inUse;

    private void Start()
    {
        shipCam.SetActive(false);
        playerCam.SetActive(true);
    }
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.E) && isIn && !inUse)
        {
            inUse = true;

            shipCam.SetActive(true);
            playerCam.SetActive(false);

            sc.lockedMove = true;
            weapon.canShoot = true;
        }
        else if(Input.GetKeyDown(KeyCode.E) && inUse)
        {
            inUse = false;

            shipCam.SetActive(false);
            playerCam.SetActive(true);

            sc.lockedMove = false;
            weapon.canShoot = false;
        }
    }
    #region Triggers
    private void OnTriggerEnter2D(Collider2D collision)
    {
        PlayerControler pc = collision.GetComponent<PlayerControler>();
        if(pc != null)
        {
            isIn = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        PlayerControler pc = collision.GetComponent<PlayerControler>();
        if(pc != null)
        {
            isIn = false;
        }
    }
    #endregion
}

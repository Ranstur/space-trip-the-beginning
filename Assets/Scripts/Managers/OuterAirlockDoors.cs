﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OuterAirlockDoors : MonoBehaviour
{
    public bool powered;
    public bool isOpen;
    [Space]
    [SerializeField] private Sprite open;
    [SerializeField] private Sprite closed;
    [Space]
    [SerializeField] private BoxCollider2D boxC;

    private SpriteRenderer sr;

    private void Start()
    {
        sr = GetComponent<SpriteRenderer>();

        isOpen = false;
    }
    private void Update()
    {
        if (powered)
        {
            if (isOpen)
            {
                sr.sprite = open;
                boxC.enabled = false;
            }
            if(!isOpen)
            {
                sr.sprite = closed;
                boxC.enabled = true;
            }
        }
    }
}

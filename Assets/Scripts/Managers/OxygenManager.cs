﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class OxygenManager : MonoBehaviour
{
    public bool powered = true;
    [Space]
    public bool mainPower;
    public bool secPower;
    [Space]
    [SerializeField] private int drainTime;
    [SerializeField] private int chargeTime;
    [SerializeField] private int maxOxygen;
    [Space]
    [SerializeField] private GameObject oxygenHub;
    [SerializeField] private TextMeshProUGUI oxygenText;
    [Space]
    [SerializeField] private ComputerUI comUI;
    [Header("Do not change!")]
    public float oxygenCount;

    private AlarmManager am;
    private TextMeshProUGUI text;
    private bool overRide;
    private bool locked;
    private bool allReadyOn;

    private void Start()
    {
        am = FindObjectOfType<AlarmManager>();
        text = oxygenHub.GetComponent<TextMeshProUGUI>();
        oxygenCount = maxOxygen;
    }
    private void Update()
    {
        text.text = $"{oxygenCount}";
        if (!overRide)
        {
            if (!mainPower && !secPower && !locked)
            {
                locked = true;
                PoweredDown();
            }
            if (mainPower || secPower && locked)
            {
                locked = false;
                PoweredUp();
            }
        }

        if(oxygenCount >= maxOxygen)
        {
            oxygenCount = maxOxygen;
        }
        if(oxygenCount <= 0)
        {
            oxygenCount = 0;

            Death gameOver = FindObjectOfType<Death>();
            gameOver.Died();
        }
    }
    public void PoweredUp()
    {
        allReadyOn = false;

        if (!powered)
        {
            am.alarmCounter -= 1;
        }
        comUI.isOn = true;

        powered = true;

        oxygenHub.SetActive(false);

        StopCoroutine(UsingAir());
        StartCoroutine(MakingAir());
    }
    public void PoweredDown()
    {
        if (!allReadyOn)
        {
            allReadyOn = true;

            if (powered)
            {
                am.alarmCounter += 1;
            }
            comUI.isOn = false;

            powered = false;

            oxygenHub.SetActive(true);

            StopCoroutine(MakingAir());
            StartCoroutine(UsingAir());
        }
    }
    public void SetOverRide(bool value)
    {
        if (value)
        {
            overRide = true;
        }
        if(!value)
        {
            overRide = false;
        }

    }
    IEnumerator MakingAir()
    {
        yield return new WaitForSeconds(chargeTime);

        oxygenCount += 1;

        if(oxygenCount >= maxOxygen)
        {
            oxygenCount = maxOxygen;
        }
        else if(powered)
        {
            StartCoroutine(MakingAir());
        }
    }
    IEnumerator UsingAir()
    {
        yield return new WaitForSeconds(drainTime);

        oxygenCount -= 1;

        if(!powered)
        {
            StartCoroutine(UsingAir());
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondaryEngine : MonoBehaviour
{
    public bool powered;
    public bool running;
    public bool activated;
    public float enginePower;
    [Header("Computer UIs")]
    [SerializeField] private ComputerUI comUI1;
    [SerializeField] private ComputerUI comUI2;
    [Space]
    [SerializeField] private ComputerUI comUIPower;

    private void Update()
    {
        if (running)
        {
            comUI1.isOn = true;
            comUI2.isOn = true;
        }
        if (!running)
        {
            comUI1.isOn = false;
            comUI2.isOn = false;
        }
        if(powered)
        {
            comUIPower.isOn = true;
        }
        if(!powered)
        {
            comUIPower.isOn = false;
        }

        if (running && powered)
        {
            activated = true;
        }
        else
        {
            activated = false;
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondaryPowerGrid : MonoBehaviour
{
    public bool powered;
    public bool powerOff;
    [Space]
    [SerializeField] private List<Computer> computers;
    [SerializeField] private ComputerUI comUI;

    private void Start()
    {
        PoweredUp();
    }
    public void PoweredUp()
    {
        powerOff = false;
        comUI.isOn = true;

        TurretManager tm = FindObjectOfType<TurretManager>();
        tm.secPower = true;

        AlarmManager am = FindObjectOfType<AlarmManager>();
        am.secPower = true;

        OxygenManager om = FindObjectOfType<OxygenManager>();
        om.secPower = true;

        foreach (Computer allComputers in computers)
        {
            allComputers.secondaryPower = true;
        }
    }
    public void PoweredDown()
    {
        powerOff = true;
        comUI.isOn = false;

        TurretManager tm = FindObjectOfType<TurretManager>();
        tm.secPower = false;

        AlarmManager am = FindObjectOfType<AlarmManager>();
        am.secPower = false;

        OxygenManager om = FindObjectOfType<OxygenManager>();
        om.secPower = false;

        foreach (Computer allComputers in computers)
        {
            allComputers.secondaryPower = false;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour
{
    public bool powered;
    public bool charged;
    public bool running;
    [Space]
    [SerializeField] private ShieldBattery sb;
    [SerializeField] public GameObject shield;
    private void Update()
    {
        if(running && charged)
        {
            charged = false;

            shield.SetActive(true);

            sb.DrainDown();
        }
        if(!powered)
        {
            sb.powered = false;
        }
        else if(powered)
        {
            sb.powered = true;
        }
    }
}

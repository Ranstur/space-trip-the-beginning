﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretManager : MonoBehaviour
{
    [SerializeField] private List<Turrets> turrets;
    [SerializeField] private ComputerUI comUI;
    [Space]
    public bool mainPower;
    public bool secPower;

    private void Update()
    {
        if(!mainPower && !secPower)
        {
            PoweredDown();
        }
        if(mainPower || secPower)
        {
            PoweredUp();
        }
    }
    public void Activated()
    {
        foreach(Turrets allTurrets in turrets)
        {
            allTurrets.isActivated = true;
        }
        comUI.isOn = true;
    }
    public void Deactivated()
    {
        foreach(Turrets allTurrets in turrets)
        {
            allTurrets.isActivated = false;
        }
        comUI.isOn = false;
    }
    public void PoweredUp()
    {
        foreach (Turrets allTurrets in turrets)
        {
            allTurrets.powered = true;
        }  
    }
    public void PoweredDown()
    {
        foreach (Turrets allTurrets in turrets)
        {
            allTurrets.powered = false;
        }
    }
}

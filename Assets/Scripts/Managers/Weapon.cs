﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public bool powered;
    public bool canShoot;
    [Space]
    [SerializeField] private Transform shotSpot;
    [SerializeField] private GameObject bulletPrefab;
    [Header("Computer UIs")]
    [SerializeField] private ComputerUI comUI1;
    [SerializeField] private ComputerUI comUI2;
    [Header("Scripts")]
    [SerializeField] private AmmoFactory af;
    [Header("Sounds")]
    [SerializeField] private AudioSource shooting;

    private void Update()
    {
        if(powered)
        {
            comUI1.isOn = true;
            comUI2.isOn = true;
        }
        if(!powered)
        {
            comUI1.isOn = false;
            comUI2.isOn = false;
        }
        if(powered && canShoot)
        {
            if(Input.GetKeyDown(KeyCode.Space))
            {
                if (af.currentAmmo >= 1)
                {
                    af.currentAmmo -= 1;

                    Shoot();
                }
            }
        }
    }
    private void Shoot()
    {
        GameObject bullet = Instantiate(bulletPrefab);
        bullet.transform.position = shotSpot.transform.position;
        bullet.transform.rotation = shotSpot.transform.rotation;

        shooting.Play();
    }
}

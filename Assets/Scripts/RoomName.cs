using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RoomName : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI floorName;

    private void Start()
    {
        floorName.text = gameObject.name;
    }
}

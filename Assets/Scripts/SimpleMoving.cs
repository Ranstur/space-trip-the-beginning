using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleMoving : MonoBehaviour
{
    [Header("platform movement")]
    public float moveSpeed = 1;
    [SerializeField] private Transform start;
    [SerializeField] private Transform end;

    private bool locked;
    private Transform target;
    private bool toEnd;

    #region platform movement
    private void Start()
    {
        //set the position at the very first node
        transform.position = start.position;
        target = end;
    }
    private void FixedUpdate()
    {
        if (!start || !end)
        {
            Debug.LogError("Hey randy, all the platforms aren't assigned to the list lol");
            return;
        }

        MoveToNextPlatform();
    }
    void MoveToNextPlatform()
    {
        //figure out what transform we are targetting
        //Transform targetTransform = end;

        //get the distance to the next platform
        float distance = Vector2.Distance(transform.position, target.position);

        //if the distance is close enough, then we can target the next platform.
        if (distance <= 0.1f)
        {
            changer(toEnd);
        }
        else //else we are not close enough, so move towards the current target platform
        {
            //move to target position
            transform.position = Vector2.MoveTowards(transform.position, target.position, moveSpeed * Time.deltaTime);
        }
    }
    private void changer(bool ended)
    {
        if (ended)
        {
            target = end;

            toEnd = false;
        }
        else
        {
            target = start;

            toEnd = true;
        }
    }
    #endregion
}

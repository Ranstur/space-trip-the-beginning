using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleShooting : MonoBehaviour
{
    [SerializeField] private GameObject bulletPrefab;
    [SerializeField] private Transform shotSpot;
    [SerializeField] private float delayTimer;

    private void Start()
    {
        StartCoroutine(Shooting());
    }
    IEnumerator Shooting()
    {
        GameObject bullet = Instantiate(bulletPrefab);
        bullet.transform.position = shotSpot.transform.position;
        bullet.transform.rotation = shotSpot.transform.rotation;

        yield return new WaitForSeconds(delayTimer);

        StartCoroutine(Shooting());
    }
}

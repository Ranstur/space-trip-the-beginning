using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackStory : MonoBehaviour
{
    public bool starting = false;
    [Space]
    [SerializeField] private GameObject cutSceneCam;
    [SerializeField] private List<GameObject> enemyShips;
    [Space]
    [SerializeField] private float camTimer;
    [Space]
    [SerializeField] private GameObject playerCam;
    [SerializeField] private GameObject shipCam;
    [Space]
    [SerializeField] private float stageTimer = 20;

    private bool locked = false;
    private bool playerCamIsTrue;
    private bool shipCamIsTrue;

    private PlayerControler pc;
    private ShipController sc;

    private GameLogic gameLogic;
    private NatralProblems np;

    private void Start()
    {
        gameLogic = FindObjectOfType<GameLogic>();
        np = FindObjectOfType<NatralProblems>();
    }
    private void Update()
    {
        if(starting && !locked)
        {
            locked = true;

            gameLogic.lockedOut = true;
            np.lockedOut = true;

            EnemiesSpawn();
            StartCoroutine(StartNextStage());
        }
    }

    private void EnemiesSpawn()
    {
        if(playerCam.activeSelf)
        {
            pc = FindObjectOfType<PlayerControler>();
            pc.canMove = false;

            playerCamIsTrue = true;
        }
        else if(shipCam.activeSelf)
        {
            sc = FindObjectOfType<ShipController>();
            sc.canMove = false;

            shipCamIsTrue = true;
        }

        playerCam.SetActive(false);
        shipCam.SetActive(false);

        foreach(GameObject allEnemies in enemyShips)
        {
            allEnemies.SetActive(true);
        }

        cutSceneCam.SetActive(true);

        StartCoroutine(DelayTime());
    }
    IEnumerator DelayTime()
    {
        yield return new WaitForSeconds(camTimer);

        if(playerCamIsTrue)
        {
            playerCam.SetActive(true);

            pc.canMove = true;
        }
        else if(shipCamIsTrue)
        {
            sc.canMove = true;

            shipCam.SetActive(true);
        }

        cutSceneCam.SetActive(false);
    }
    IEnumerator StartNextStage()
    {
        yield return new WaitForSeconds(stageTimer);

        SpaceRockCrash src = FindObjectOfType<SpaceRockCrash>();
        src.starting = true;
    }
}

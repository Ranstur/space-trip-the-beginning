using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Credits : MonoBehaviour
{
    [SerializeField] private float speed;
    [Space]
    [SerializeField] private float holdTimer;
    [SerializeField] private float wholeTimer;

    private bool move;

    private void Start()
    {
        StartCoroutine(StartTimer());
        StartCoroutine(LastTimer());
    }
    private void Update()
    {
        if (move)
        {
            transform.Translate(0, Time.deltaTime * speed, 0);
        }
    }
    IEnumerator StartTimer()
    {
        yield return new WaitForSeconds(holdTimer);

        move = true;
    }
    IEnumerator LastTimer()
    {
        yield return new WaitForSeconds(wholeTimer);

        SceneManager.LoadScene(0);
    }
}

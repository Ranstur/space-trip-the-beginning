using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetCrashShip : MonoBehaviour
{
    public bool starting;
    [Space]
    [SerializeField] private float speed;
    [SerializeField] private float shrinkSpeed;
    [Space]
    [SerializeField] private float timing = 1;
    [SerializeField] private int scaleSizeTimer;
    [Space]
    [SerializeField] private GameObject exsplotionPerfab;

    private bool locked;
    private bool stoped;

    private void Update()
    {
        if (starting && !stoped)
        {
            transform.Translate(0, Time.deltaTime * speed, 0);

            if(!locked)
            {
                locked = true;

                StartCoroutine(Shrinking());

                StartCoroutine(TripOut());
            }
        }
    }
    IEnumerator Shrinking()
    {
        transform.localScale -= new Vector3(shrinkSpeed, shrinkSpeed, 0);

        yield return new WaitForSeconds(timing);

           StartCoroutine(Shrinking());
    }
    IEnumerator TripOut()
    {
        yield return new WaitForSeconds(scaleSizeTimer);

        StopAllCoroutines();

        stoped = true;

        GameObject exsplotion = Instantiate(exsplotionPerfab);
        exsplotion.transform.position = transform.position;
        exsplotion.transform.rotation = transform.rotation;
    }
}

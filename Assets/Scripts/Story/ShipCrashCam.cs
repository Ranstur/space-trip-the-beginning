using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ShipCrashCam : MonoBehaviour
{
    public bool starting;
    [Space]
    [SerializeField] private GameObject cutSceneCam;
    [Space]
    [SerializeField] private GameObject shipCam;
    [Space]
    [SerializeField] private float timerToCredits;

    private PlanetCrashShip pcs;
    private bool locked;

    private void Update()
    {

        pcs = FindObjectOfType<PlanetCrashShip>();

        if(starting && !locked)
        {
            StartCoroutine(Credits());

            locked = true;

            cutSceneCam.SetActive(true);

            if(shipCam.activeSelf)
            {
                shipCam.SetActive(false);
            }

            pcs.starting = true;
        }
    }
    IEnumerator Credits()
    {
        yield return new WaitForSeconds(timerToCredits);

        SceneManager.LoadScene(2);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipCrashed : MonoBehaviour
{
    [SerializeField] private float delay;
    [SerializeField] private List<Sprite> sprites;
    [Space]
    [SerializeField] private AudioSource sound;

    private SpriteRenderer sr;
    private int target;

    private void Start()
    {
        sound.Play();

        sr = GetComponent<SpriteRenderer>();

        StartCoroutine(changer());
    }
    IEnumerator changer()
    {
        sr.sprite = sprites[target];

        yield return new WaitForSeconds(delay);

        target++;

        if (target >= sprites.Count)
        {
            Destroy(gameObject);
        }

        StartCoroutine(changer());
    }
}

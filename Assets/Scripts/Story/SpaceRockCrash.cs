using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceRockCrash : MonoBehaviour
{
    public bool starting;
    [Space]
    [SerializeField] private float speed;
    [SerializeField] private GameObject cutSceneCam;
    [SerializeField] private float cutSceneTimer;
    [Space]
    [SerializeField] private GameObject playerCam;
    [SerializeField] private GameObject shipCam;
    [Space]
    [SerializeField] private List<GameObject> enemyShip;

    private bool locked;
    private bool playerCamIsTrue;
    private bool shipCamIsTrue;

    private void Update()
    {
        if(starting)
        {
            transform.Translate(0, -Time.deltaTime * speed, 0);

            if(!locked)
            {
                locked = true;

                StartCoroutine(CutScene());
            }
        }
    }
    IEnumerator CutScene()
    {
        if(playerCam.activeSelf)
        {
            playerCamIsTrue = true;
            playerCam.SetActive(false);
        }
        else if(shipCam.activeSelf)
        {
            shipCamIsTrue = true;
            shipCam.SetActive(false);
        }
        cutSceneCam.SetActive(true);

        yield return new WaitForSeconds(cutSceneTimer);

        if(playerCamIsTrue)
        {
            playerCam.SetActive(true);
        }
        else if(shipCamIsTrue)
        {
            shipCam.SetActive(true);
        }
        cutSceneCam.SetActive(false);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        ShipController sc = collision.GetComponent<ShipController>();
        PlayerControler pc = FindObjectOfType<PlayerControler>();

        if(sc != null)
        {
            ShipCrashCam scc = FindObjectOfType<ShipCrashCam>();

            scc.starting = true;

            Destroy(sc.gameObject);
            Destroy(pc.gameObject);

            foreach(GameObject allShips in enemyShip)
            {
                Destroy(allShips);
            }
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial : MonoBehaviour
{
    [SerializeField] private GameObject movementUI;
    [Space]
    [SerializeField] private Computer setComputer;
    [SerializeField] private GameObject useComputerUI;
    [SerializeField] private GameObject arrowToComputer;
    [Space]
    [SerializeField] private GameObject UIHelp;

    private PlayerControler pc;
    private bool movement;
    private bool computer;
    private bool UI;

    private void Start()
    {
        pc = FindObjectOfType<PlayerControler>();
        pc.canMove = false;

        movementUI.SetActive(true);
    }
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            if (!movement)
            {
                movement = true;

                movementUI.SetActive(false);

                pc.canMove = true;

                useComputerUI.SetActive(true);
                arrowToComputer.SetActive(true);
            }
        }
        if(Input.GetKeyDown(KeyCode.E) && setComputer.isIn)
        {
            if(movement && !computer)
            {
                computer = true;

                useComputerUI.SetActive(false);
                arrowToComputer.SetActive(false);

                UIHelp.SetActive(true);
            }
            else if (movement && computer && !UI)
            {
                UI = true;

                UIHelp.SetActive(false);
            }
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Death : MonoBehaviour
{
    [SerializeField] private Button mainMenuButton;
    [SerializeField] private Button quitButton;
    [Space]
    [SerializeField] private GameObject deathUI;

    private void Start()
    {
        deathUI.SetActive(false);
        mainMenuButton.onClick.AddListener(OnMainMenu);
        quitButton.onClick.AddListener(OnQuit);
    }
    public void Died()
    {
        Time.timeScale = 0f;
        deathUI.SetActive(true);
    }
    private void OnMainMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(0);
    }
    private void OnQuit()
    {
        Application.Quit();
        Debug.Log("Now Quitting...");
    }
}

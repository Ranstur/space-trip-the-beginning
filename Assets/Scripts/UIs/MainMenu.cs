using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private Button playButton;
    [SerializeField] private Button settingsButton;
    [SerializeField] private Button quitButton;
    [Header("UIs")]
    [SerializeField] private GameObject mainMenuUI;
    [SerializeField] private GameObject settingsMenuUI;
    [SerializeField] private GameObject spaceShip;

    private void Start()
    {
        playButton.onClick.AddListener(OnPlay);
        settingsButton.onClick.AddListener(OnSettings);
        quitButton.onClick.AddListener(OnQuit);
    }
    private void OnPlay()
    {
        SceneManager.LoadScene(1);
    }
    private void OnSettings()
    {
        settingsMenuUI.SetActive(true);
        mainMenuUI.SetActive(false);
        spaceShip.SetActive(false);
    }
    private void OnQuit()
    {
        Application.Quit();
        Debug.Log("Qutting...");
    }
}

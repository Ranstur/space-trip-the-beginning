using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] private Button resumeButton;
    [SerializeField] private Button quitButton;
    [Space]
    [SerializeField] private GameObject pauseMenuUI;

    private bool paused;

    private void Start()
    {
        resumeButton.onClick.AddListener(OnResume);
        quitButton.onClick.AddListener(OnQuit);
    }
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if (!paused)
            {
                Paused();
            }
            else OnResume();
        }
    }
    private void Paused()
    {
        paused = true;
        Time.timeScale = 0f;
        pauseMenuUI.SetActive(true);
    }
    private void OnResume()
    {
        paused = false;
        Time.timeScale = 1f;
        pauseMenuUI.SetActive(false);
    }
    private void OnQuit()
    {
        Application.Quit();
        Debug.Log("Shutting Down....");
    }
}

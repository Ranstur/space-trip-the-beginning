﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AlarmLights : MonoBehaviour
{
    public bool powered;
    public bool isActivated;
    public bool running;
    [Space]
    [SerializeField] private AudioSource syran;

    private bool isOn;
    private void Start()
    {
        syran = GetComponent<AudioSource>();
        syran.playOnAwake = false;
    }
    private void Update()
    {
        if (isActivated)
        {
            if (running && !isOn && powered)
            {
                isOn = true;
                syran.Play();
            }
            else if (!running && isOn)
            {
                isOn = false;
                syran.Stop();
            }
            else if(!powered && isOn)
            {
                isOn = false;
                syran.Stop();
            }
        }
        if(!isActivated)
        {
            syran.Stop();
        }
    }
}

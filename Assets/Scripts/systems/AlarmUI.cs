﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlarmUI : MonoBehaviour
{
    public bool running;
    [Space]
    [SerializeField] private GameObject alarmRed;
    [SerializeField] private float flashSpeed = 0.1f;

    private bool isOn;
    private void Update()
    {
        if(running && !isOn)
        {
            isOn = true;

            StartCoroutine(FlashLight());
        }
        if(!running && isOn)
        {
            isOn = false;

            alarmRed.SetActive(false);

            StopAllCoroutines();
        }
    }
    IEnumerator FlashLight()
    {
        alarmRed.SetActive(true);

        yield return new WaitForSeconds(flashSpeed);

        alarmRed.SetActive(false);

        yield return new WaitForSeconds(flashSpeed);

        if(running)
        {
            StartCoroutine(FlashLight());
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(AudioSource))]
public class Turrets : MonoBehaviour
{
    public bool powered;
    public bool isActivated;
    [Space]
    [SerializeField] private int damage;
    [Header("Effects")]
    [SerializeField] private ParticleSystem muzzleFlash;
    [SerializeField] private float delayToNextFlash;
    [SerializeField] private AudioSource gunSound;

    private Enemy targetEnemy;
    private bool attack;
    private bool isOn;

    private void Start()
    {
        gunSound = GetComponent<AudioSource>();
        gunSound.playOnAwake = false;
    }
    private void Update()
    {
        if (isActivated)
        {
            if (powered && attack && !isOn)
            {
                isOn = true;
                StartCoroutine(MuzzleFlash());
            }
            else if (!powered && attack && isOn || powered && !attack && isOn)
            {
                isOn = false;
                StopAllCoroutines();
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Enemy enemy = collision.GetComponent<Enemy>();
        targetEnemy = enemy;
        if(enemy != null && powered)
        {
            attack = true;
            enemy.TakeDamage(damage);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        Enemy enemy = collision.GetComponent<Enemy>();
        if(enemy != null)
        {
            attack = false;
            StopAllCoroutines();
            targetEnemy = null;
        }
    }
    private IEnumerator MuzzleFlash()
    {
        muzzleFlash.Play();
        gunSound.Play();

        yield return new WaitForSeconds(delayToNextFlash);

        if (isActivated)
        {
            if (powered && attack)
            {
                targetEnemy.TakeDamage(damage);
                StartCoroutine(MuzzleFlash());
            }
        }
    }
}
